# full_oppo6750_17351-user 7.1.1 N4F26M 1593605483 release-keys
full_oppo6750_17351-user 7.1.1 N4F26M 1593605483 release-keys
- manufacturer: OPPO
- platform: mt6750
- codename: CPH1717
- flavor: full_oppo6750_17351-user
- release: 7.1.1
- id: N4F26M
- incremental: 1593605483
- tags: release-keys
- fingerprint: 
- is_ab: false
- brand: OPPO
- branch: full_oppo6750_17351-user-7.1.1-N4F26M-1593605483-release-keys
- repo: oppo_cph1717_dump
